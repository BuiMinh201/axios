import { useEffect, useState } from "react";
import { GetPost } from "../../services/Api";
const Post = () => {
    const [posts,setPost] = useState([])
    useEffect(()=>{
        GetPost()
        .then(({data})=>setPost(data))
        .catch()
    },[])
  return (
    <table className="table table-dark table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Title</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody>
      {
        posts.map((post,index)=>
        <tr key={index}>
          <td>{post.id}</td>
          <td>{post.title}</td>
          <td>{post.body}</td>
        </tr>
       )
        }
      </tbody>
    </table>
  );
};
export default Post;
