import { useEffect, useState } from "react";
import { GetUser } from "../../services/Api";

const User = () => {
    const [users,setUsers] = useState([])
    useEffect(()=>{
        GetUser()
        .then(({data})=>setUsers(data))
        .catch()
    },[])
  return (
    <table className="table table-dark table-hover">
      <thead>
        <tr>
          <th>Fullname</th>
          <th>Username</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>
        {
            users.map((user,index)=>( 
        <tr key={index}>
          <td>{user.name}</td>
          <td>{user.username}</td>
          <td>{user.email}</td>
        </tr>)
       )
        }

      </tbody>
    </table>
  );
};
export default User;
