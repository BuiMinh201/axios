import Header from "./Shared/components/Layout/Header";
import Menu from "./Shared/components/Layout/Menu";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import User from "./pages/User";
import Post from "./pages/Post";

const App = () => {
  return (
    <>
      <BrowserRouter>
        <div id="wrapper" className="container">
          <Header />
          <Menu />
          <Routes>
            <Route path="/" element={<User />} />
            <Route path="/Post" element={<Post />} />
          </Routes>
        </div>
      </BrowserRouter>
    </>
  );
};
export default App;
